'use strict';

var _ = require('lodash');

var ZENCART_IMPORT_TAG = 'ZenCart_Import';

function zenCart_Shopify_Redirect_library() {
    // allow either
    //   var zenCart_Shopify_Redirect_library = require('./index);
    //
    //   zenCart_Shopify_Redirect_library = zenCart_Shopify_Redirect_library();
    // or
    //   zenCart_Shopify_Redirect_library = new zenCart_Shopify_Redirect_library();
    if (!(this instanceof zenCart_Shopify_Redirect_library)) {
        return new zenCart_Shopify_Redirect_library();
    }

    if (this instanceof zenCart_Shopify_Redirect_library) {
        var me = this;

        this.tagToZencartCategoryUrl = {};
        _.forOwn(categoryToTagMappings, function(categoryProperties, category) {
            me.tagToZencartCategoryUrl[categoryProperties.tag] =
            {url: categoryProperties.url, parent: categoryProperties.parent};
        });

        this.urlToZencartUrlMap = {};
        _.forOwn(categoryToTagMappings, function(categoryProperties, category) {
            me.urlToZencartUrlMap[categoryProperties.url] =
            {url: categoryProperties.url, parent: categoryProperties.parent};
        });
    }
}

/**
 *  Defines the mappings from Zencart category name to Shopify tag and Zencart url.
 */
var categoryToTagMappings = {
    accessories:    {tag:'Accessories', url: 'accessories', parent: true},
        Eyewear:        {tag:'Eyewear',     url: 'accessories/eyewear',     parent: false},
        "Hair Clips":   {tag:'Barrettes',   url: 'accessories/hair-clips',  parent: false},
        Handbags:       {tag:'Handbags',    url: 'accessories/handbags',    parent: false},
        Jewelry:        {tag:'Jewelry',     url: 'accessories/jewelry',     parent: false},
        "Other bags":   {tag:'Other Bags',  url: 'accessories/other-bags',  parent: false},
        Scarves:        {tag:'Scarves',     url: 'accessories/scarves',     parent: false},
        Wallets:        {tag:'Wallets',     url: 'accessories/wallets',     parent: false},
        Watches:        {tag:'Watches',     url: 'accessories/watches',     parent: false},

    gifts:   {tag:'Gifts',  url: 'gifts', parent: true},
        "Calendars & Planners": {tag:'Calendars & Planners',    url: 'gifts/calendars-planners',    parent: false},
        Candles:                {tag:'Candles',                 url: 'gifts/candles',               parent: false},
        "Lotions & Fragrance":  {tag:'Lotions & Fragrance',     url: 'gifts/lotions-fragrance',     parent: false},
        "Misc.":                {tag:'Misc Gifts',              url: 'gifts/misc.',                 parent: false},
        Notebooks:              {tag:'Notebooks',               url: 'gifts/notebooks',             parent: false},
        Soaps:                  {tag:'Soaps',                   url: 'gifts/soaps',                 parent: false},
        Stationary:             {tag:'Stationary',              url: 'gifts/stationary',            parent: false},

    home: {tag:'Home Decor', url: 'home',           parent: true},
    Home: {tag:'Home Decor', url: 'home',           parent: true},
        Art:    {tag:'Art & Prints',        url: 'home/art',    parent: false},
        Frames: {tag:'Vases & Frames',      url: 'home/frames', parent: false},
        Prints: {tag:'Art & Prints',        url: 'home/prints', parent: false},
        Vases:  {tag:'Vases & Frames',      url: 'home/vases',  parent: false},

    tabletop: {tag:'Tabletop', url: 'tabletop', parent: true},
        "Bowls/Plates":     {tag:'Bowls & Plates',      url: 'tabletop/bowls-plates',       parent: false},
        "Coasters/Trivets": {tag:'Coasters & Trivets',  url: 'tabletop/coasters-trivets',   parent: false},
        "Mugs/Glasses":     {tag:'Mugs & Glasses',      url: 'tabletop/mugs-glasses',       parent: false},
        Placemats:          {tag:'Placemats',           url: 'tabletop/placemats',          parent: false},
        "Tea Towels":       {tag:'Tea Towels',          url: 'tabletop/tea-towels',         parent: false}
};

/**
 * Returns the Shopify product tag for products imported from Zencart.
 */
zenCart_Shopify_Redirect_library.prototype.zencartImportTag = function() {
    return ZENCART_IMPORT_TAG;
};

/**
 * Checks if a url is a from an imported Zencart product.
 */
zenCart_Shopify_Redirect_library.prototype.isZenCartUrl = function(url) {
    var me = this;
    var isZenCartUrl = false;
    var urlParts = url.split('/');

    if(urlParts[1] && urlParts[2]) {
        var parentPart = urlParts[1];
        var zenCartUrl = urlParts[1] + '/' + urlParts[2];

        if(me.urlToZencartUrlMap.hasOwnProperty(parentPart) && me.urlToZencartUrlMap[parentPart].parent && me.urlToZencartUrlMap.hasOwnProperty(zenCartUrl))
        {
            isZenCartUrl = true;
        }
    }

    return isZenCartUrl;
};

/**
 * Maps a Zencart category name to a Shopify tag.
 */
zenCart_Shopify_Redirect_library.prototype.mapCategoryToTag = function(category) {
    var tag = category;

    if (categoryToTagMappings[category]) {
        tag = categoryToTagMappings[category].tag;
    }

    return tag;
};

/**
 * Maps Shopify product tags to a Zencart categories url.
 */
zenCart_Shopify_Redirect_library.prototype.mapTagsToZencartUrl = function(tagsString) {
    var me = this;
    var tags = tagsString.split(',');
    var zencartUrl = 'home';
    var hasChildTag = false;

    tags.forEach(function(tag) {
        var trimmedTag = tag.trim();
        if(me.tagToZencartCategoryUrl[trimmedTag]) {    // process only known tags
            if(!hasChildTag) {                // skip the rest of the tags if a sub-category tag has already be processed.
                zencartUrl = me.tagToZencartCategoryUrl[trimmedTag].url;
                hasChildTag = !me.tagToZencartCategoryUrl[trimmedTag].parent;
            }
        }
    });

    return zencartUrl;
};

/**
 * Export the zenCart_Shopify_Redirect_library module.
 */
module.exports = zenCart_Shopify_Redirect_library;
