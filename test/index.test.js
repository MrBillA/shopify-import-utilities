/*!
 *
 *
 * Bill Andrews <billa@22fish.net>
 * MIT License.
 */

/**
 * Test dependencies.
 */
var redirects_util = require('../index'),
    _ = require('lodash'),
    chai = require('chai'),
    assert = chai.assert;

var should = chai.should();
var assert = chai.assert;

describe('util tests', function() {
    it('library export should return an object and not the constructor function', function () {
        assert(_.isFunction(redirects_util));
        assert(!_.isFunction(redirects_util()));
        assert(_.isObject(redirects_util()));
    });

    it('zencartImportTag should return correct value.', function () {
        var lib = redirects_util();

        lib.zencartImportTag().should.be.equal('ZenCart_Import');
    });


    it('isZenCartUrl should detect zencart urls.', function () {
        var lib = redirects_util();

        lib.isZenCartUrl('').should.be.equal(false);
        lib.isZenCartUrl('tabletop/bowls-plates').should.be.equal(false);
        lib.isZenCartUrl('/tabletop/bowls-plates').should.be.equal(true);
        lib.isZenCartUrl('/accessories/other-bags').should.be.equal(true);
        lib.isZenCartUrl('/accessories/other-bags/otherstuff').should.be.equal(true);
    });

    it('mapCategoryToTag returns correct tags', function () {
        var lib = redirects_util();

        lib.mapCategoryToTag('accessories').should.be.equal('Accessories');
        lib.mapCategoryToTag('Hair Clips').should.be.equal('Barrettes');
        lib.mapCategoryToTag('Other bags').should.be.equal('Other Bags');
        lib.mapCategoryToTag('Misc.').should.be.equal('Misc Gifts');
        lib.mapCategoryToTag('Bowls/Plates').should.be.equal('Bowls & Plates');
        lib.mapCategoryToTag('Coasters/Trivets').should.be.equal('Coasters & Trivets');
        lib.mapCategoryToTag('Mugs/Glasses').should.be.equal('Mugs & Glasses');
        lib.mapCategoryToTag('Art').should.be.equal('Art & Prints');
        lib.mapCategoryToTag('Prints').should.be.equal('Art & Prints');
        lib.mapCategoryToTag('Frames').should.be.equal('Vases & Frames');
        lib.mapCategoryToTag('Vases').should.be.equal('Vases & Frames');
    });

    it('mapTagsToZencartUrl returns correct urls', function () {
        var lib = redirects_util();

        lib.mapTagsToZencartUrl('Accessories').should.be.equal('accessories');
        lib.mapTagsToZencartUrl('Accessories, Scarves').should.be.equal('accessories/scarves');
        lib.mapTagsToZencartUrl('Gifts, Misc Gifts').should.be.equal('gifts/misc.');
    });
});
